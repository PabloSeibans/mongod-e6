db.getCollection("familia_fernandez").insertMany([
                                                {
                                                    "Nombre":"Eleuteria Aduviri",
                                                    "rolFamiliar": "Madre",
                                                    "Edad": 40
                                                },
                                                {
                                                    "Nombre":"Emilio Fernandez Ticona",
                                                    "rolFamiliar": "Padre",
                                                    "Edad": 45
                                                },
                                                {
                                                    "Nombre":"Elizabeth Fernandez Aduviri",
                                                    "rolFamiliar": "Hija Mayor",
                                                    "Edad": 28
                                                },
                                                {
                                                    "Nombre":"Pablo Fernandez Aduviri",
                                                    "rolFamiliar": "Hijo Medio",
                                                    "Edad": 21
                                                },
                                                {
                                                    "Nombre":"Keyla Fernandez Aduviri",
                                                    "rolFamiliar": "Hija Menor",
                                                    "Edad": 4
                                                }
                                            ])
                                                

db.getCollection('familia_fernandez').find({})

                         
db.getCollection("familia_fernandez").createIndex({"Nombre":1})
db.getCollection("familia_fernandez").createIndex({"rolFamiliar":1},{unique: true})


db.getCollection("familia_fernandez").find({"Nombre" : "Pablo Fernandez Aduviri"})